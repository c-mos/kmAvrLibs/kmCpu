// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
/** @file
* @brief CPU auxiliary library definitions for AVR MCUs.
* - kmCpu.h
* - kmCpuDefs.h
*
*  **Created on**: Jul 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmCpu library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KM_CPUDEFS_H_
#define KM_CPUDEFS_H_

#include "../kmCommon/kmCommon.h"

/// CPU Clock at    32 768 Hz
#define KM_CPU_CLOCK_STD_32768_HZ	     32768   //     32.768 KHz
/// CPU Clock at  1 000 000 Hz
#define KM_CPU_CLOCK_STD_1000000_HZ   1000000   //   1 MHz
/// CPU Clock at  1 843 200 Hz
#define KM_CPU_CLOCK_STD_1843200_HZ   1843200   //   1.8432 MHz
/// CPU Clock at  2 000 000 Hz
#define KM_CPU_CLOCK_STD_2000000_HZ   2000000   //   2 MHz
/// CPU Clock at  3 686 400 Hz
#define KM_CPU_CLOCK_STD_3686400_HZ   3686400   //   3.6864 MHz
/// CPU Clock at  4 000 000 Hz
#define KM_CPU_CLOCK_STD_4000000_HZ   4000000   //   4 MHz
/// CPU Clock at  7 372 800 Hz
#define KM_CPU_CLOCK_STD_7372800_HZ   7372800   //   7.3728 MHz
/// CPU Clock at  8 000 000 Hz
#define KM_CPU_CLOCK_STD_8000000_HZ   8000000   //   8 MHz
/// CPU Clock at  9 000000 Hz
#define KM_CPU_CLOCK_STD_9000000_HZ   9000000   //   9 MHz
/// CPU Clock at 11 059 200 Hz
#define KM_CPU_CLOCK_STD_11059200_HZ 11059200   //  11.0592 MHz
/// CPU Clock at 14 745 600 Hz
#define KM_CPU_CLOCK_STD_14745600_HZ 14745600   //  14.7456 MHz
/// CPU Clock at 16 000 000 Hz
#define KM_CPU_CLOCK_STD_16000000_HZ 16000000   //  16 MHz
/// CPU Clock at 18 432 000 Hz
#define KM_CPU_CLOCK_STD_18432000_HZ 18432000   //  18.4320 MHz
/// CPU Clock at 20 000 000 Hz
#define KM_CPU_CLOCK_STD_20000000_HZ 20000000   //  20 MHz
/// CPU Clock at 24 000 000 Hz
#define KM_CPU_CLOCK_STD_24000000_HZ 24000000   //  24 MHz
/// CPU Clock at 25 000000 Hz
#define KM_CPU_CLOCK_STD_25000000_HZ 25000000   //  25 MHz

/// AVR CPU Series 0
#if \
	defined(__AVR_ATmega8__) \
	|| defined(__AVR_ATmega8A__) \
	|| defined(__AVR_ATmega16__) \
	|| defined(__AVR_ATmega16A__) \
	|| defined(__AVR_ATmega32__) \
	|| defined(__AVR_ATmega32A__)
#define KM_CPU_AVR_SERIES_0
#endif

#if \
	defined(__AVR_ATmega8__) \
	|| defined(__AVR_ATmega8A__) \
	|| defined(__AVR_ATmega16__) \
	|| defined(__AVR_ATmega16A__)
#define KM_CPU_AVR_LAYOUT_28
#endif

#if \
	defined(__AVR_ATmega32__) \
	|| defined(__AVR_ATmega32A__)
#define KM_CPU_AVR_LAYOUT_40
#endif

/// AVR CPU Series 4
#if \
	defined(__AVR_ATmega644__) \
	|| defined(__AVR_ATmega644P__) \
	|| defined(__AVR_ATmega1284__) \
	|| defined(__AVR_ATmega1284P__)
#define KM_CPU_AVR_SERIES_4
#define KM_CPU_AVR_SERIES_48
#define KM_CPU_AVR_LAYOUT_40
#endif

/// AVR CPU Series 8
#if \
	defined(__AVR_ATmega328__) \
	|| defined(__AVR_ATmega328P__) \
	|| defined(__AVR_ATmega328PB__)
#define KM_CPU_AVR_SERIES_8
#define KM_CPU_AVR_SERIES_48
#define KM_CPU_AVR_LAYOUT_28
#endif

/// AVR CPU Series RFR2
#if \
	defined(__AVR_ATmega256RFR2__)
#define KM_CPU_AVR_SERIES_8
#define KM_CPU_AVR_SERIES_48
#define KM_CPU_AVR_LAYOUT_64
#endif

/// AVR CPU Series 256x
#if \
	defined(__AVR_ATmega2560__)
#define KM_CPU_AVR_SERIES_8
#define KM_CPU_AVR_SERIES_48
#define KM_CPU_AVR_LAYOUT_100
#endif

#endif /* KM_CPUDEFS_H_ */
