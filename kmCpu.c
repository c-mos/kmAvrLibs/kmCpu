/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* kmCpu.c
*
*  **Created on**: Jul 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmCpu library for AVR MCUs
*  Copyright (C) 2019  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include <stdbool.h>

#include "kmCpu.h"
#include "kmCpuDefs.h"

void kmCpuSoftReset(void) {
	kmCpuInterruptsDisable();
	kmCpuWatchdogEnable(0);
	while(true) {} // endless loop
}

void kmCpuDisableWatchdogAndInterruptsOnStartup(void) {
	cli();
#ifdef WDIF
	wdt_reset();
	MCUSR &= ~_BV(WDRF);
	WDTCSR |= _BV(WDCE) | _BV(WDE);
	WDTCSR = 0x00;
#endif
}

void kmCpuHalt(void) {
	kmCpuWatchdogDisable();
	kmCpuInterruptsDisable();
	while(true) {}
}


#ifdef WDIF
void __init3( void ) __attribute__ (( section( ".init3" ), naked, used ));
void __init3( void ) {
	kmCpuDisableWatchdogAndInterruptsOnStartup();
}
#endif