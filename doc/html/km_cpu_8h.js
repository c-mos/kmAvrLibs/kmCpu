var km_cpu_8h =
[
    [ "kmCpuDisableWatchdogAndInterruptsOnStartup", "km_cpu_8h.html#ae403db6f1fe7e2bc5f6e0319c340fbf4", null ],
    [ "kmCpuHalt", "km_cpu_8h.html#a2cab7faaa3a7f692118e494d9bdca404", null ],
    [ "kmCpuInterruptsDisable", "km_cpu_8h.html#a89ffdd67aa53e2885528d1b820513386", null ],
    [ "kmCpuInterruptsEnable", "km_cpu_8h.html#a0091ca9e864da5359580d4a9e7b828d9", null ],
    [ "kmCpuSoftReset", "km_cpu_8h.html#accc2b1261d0ba57b5dbf5476482f9534", null ]
];