var km_cpu_defs_8h =
[
    [ "KM_CPU_CLOCK_STD_1000000_HZ", "km_cpu_defs_8h.html#a8b212649bf8551df7b45446530cd236e", null ],
    [ "KM_CPU_CLOCK_STD_11059200_HZ", "km_cpu_defs_8h.html#a15ae0ddc5604f1c8098c9710f311a797", null ],
    [ "KM_CPU_CLOCK_STD_14745600_HZ", "km_cpu_defs_8h.html#aeb5287e50ec482dc94c93334831695cc", null ],
    [ "KM_CPU_CLOCK_STD_16000000_HZ", "km_cpu_defs_8h.html#a15b748822ac89065447fb61d51b4d3c7", null ],
    [ "KM_CPU_CLOCK_STD_18432000_HZ", "km_cpu_defs_8h.html#a3d07c7d9c2b68665d036cd6f61686c55", null ],
    [ "KM_CPU_CLOCK_STD_1843200_HZ", "km_cpu_defs_8h.html#afa1f9e225912eb14f56f839673b3b3b9", null ],
    [ "KM_CPU_CLOCK_STD_20000000_HZ", "km_cpu_defs_8h.html#aae5b8e359b5150a64ce100b6c61be24c", null ],
    [ "KM_CPU_CLOCK_STD_2000000_HZ", "km_cpu_defs_8h.html#a17e11d2d85806afe16b63333ff5dac0e", null ],
    [ "KM_CPU_CLOCK_STD_24000000_HZ", "km_cpu_defs_8h.html#a934766e33f008e4d393bb0093d55b635", null ],
    [ "KM_CPU_CLOCK_STD_25000000_HZ", "km_cpu_defs_8h.html#adb35e820a0ab49de7122421d54535456", null ],
    [ "KM_CPU_CLOCK_STD_32768_HZ", "km_cpu_defs_8h.html#a550fe6b02a611769ddbfa05f7ba4ef6d", null ],
    [ "KM_CPU_CLOCK_STD_3686400_HZ", "km_cpu_defs_8h.html#abc2ad23ace1a76bb3094c60e5337d759", null ],
    [ "KM_CPU_CLOCK_STD_4000000_HZ", "km_cpu_defs_8h.html#a2f53cbefee3058307de8fbf4c92659af", null ],
    [ "KM_CPU_CLOCK_STD_7372800_HZ", "km_cpu_defs_8h.html#a0df2dfe84f80dbda443583b2aab394be", null ],
    [ "KM_CPU_CLOCK_STD_8000000_HZ", "km_cpu_defs_8h.html#ad1ee12c638b9646cade7d59fc40e6bf7", null ],
    [ "KM_CPU_CLOCK_STD_9000000_HZ", "km_cpu_defs_8h.html#a76b3918cfad839c27fe6908a02e12ba0", null ]
];