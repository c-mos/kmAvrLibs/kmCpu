# ReadMe
# kmCpu library for AVR MCUs

The kmCPU library offers a set of functions to control CPU operations and peripherals on AVR MCUs

## Table of Contents
- [Version History](#version-history)
- [Overview](#overview)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [Example Code](#example-code)
- [Author and License](#author-and-license)

## Version History
v1.0.0 Initial (2024-05-25)

## Overview
Includes:
- Functions to enable and disable interrupts (`kmCpuInterruptsEnable()`, `kmCpuInterruptsDisable()`).
- Watchdog management functions to enable, disable, and reset the watchdog timer (`kmCpuWatchdogEnable()`, `kmCpuWatchdogDisable()`, `kmCpuWatchdogReset()`).
- Additional functions for halting the CPU (`kmCpuHalt()`), performing a soft reset (`kmCpuSoftReset()`), and disabling watchdog and interrupts on startup (`kmCpuDisableWatchdogAndInterruptsOnStartup()`).


## Dependencies
- [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)


## Usage
Getting this library and adding it to own project:
- To add this module to own project as submodule - enter the main directory of the source code and use git command

``` bash
git submodule add git@gitlab.com:c-mos/kmAvrLibs/kmCpu.git kmCpu
```
- After cloning own application from git repository use following additional git command to get correct revision of submodule:
``` bash
git submodule update --init
```

### Interrupt Control

Use the functions `kmCpuInterruptsEnable()` and `kmCpuInterruptsDisable()` to respectively enable and disable interrupts.

### Watchdog Management

Manage the watchdog timer using the functions `kmCpuWatchdogEnable()`, `kmCpuWatchdogDisable()`, and `kmCpuWatchdogReset()` to enable, disable, and reset the watchdog timer.

### CPU Operations

Utilize functions like `kmCpuHalt()` to halt the CPU, `kmCpuSoftReset()` to perform a software reset, and `kmCpuDisableWatchdogAndInterruptsOnStartup()` to disable watchdog and interrupts on startup.


## Example Code
```c
#include "kmCommon/kmCommon.h"

int main(void) {
	// Init interrupts and watchdog
    kmCpuInterruptsEnable();
	kmCpuWatchdogEnable(WDTO_2S);

	while (true) {
        kmCpuWatchdogReset();
	}
}
```

## Author and License
Author: Krzysztof Moskwa

e-mail: chris[dot]moskva[at]gmail[dot]com

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)