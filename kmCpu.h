// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
/** @file
* @mainpage
* @section pageTOC Content
* @brief CPU auxiliary library functions for AVR MCUs.
* - kmCpu.h
* - kmCpuDefs.h
*
*  **Created on**: Jul 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmCpu library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KMCPU_H_
#define KMCPU_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <avr/interrupt.h>
#include <avr/wdt.h>

#include "../kmCommon/kmCommon.h"

/**
Enable interrupts.
*/
inline void kmCpuInterruptsEnable(void) {
	sei();
}

/**
Disable interrupts.
*/
inline void kmCpuInterruptsDisable(void) {
	cli();
}

/**
Enable watchdog with specific timeout
@param timeout Symbolic constants for the watchdog timeout. Since the watchdog timer is \
based on a free-running RC oscillator, the times are approximate only and apply \
to a supply voltage of 5 V. At lower supply voltages, the times will increase. \
For older devices, the times will be as large as three times when operating at \
Vcc = 3 V, while the newer devices (e. g. ATmega128, ATmega8) only experience \
a negligible change. \
Possible timeout values are: 15 ms, 30 ms, 60 ms, 120 ms, 250 ms, 500 ms, 1 s, 2 s. \
(Some devices also allow for 4 s and 8 s.(for limited devices). \
Symbolic constants are formed by the prefix WDTO_, followed by the time. \
Example that would select a watchdog timer expiry of approximately 500 ms:
kmCpuWatchdogEnable(WDTO_500MS);
*/
inline static void kmCpuWatchdogEnable(uint8_t timeout) {
	wdt_enable(timeout);
}

/**
Disable watchdog.
*/
inline static void kmCpuWatchdogDisable(void) {
	wdt_disable();
}

/**
Reset watchdog.
*/
inline static void kmCpuWatchdogReset() {
	wdt_reset();
}

/**
Disables watchdog, interrupts and enters endless loop.
*/
void kmCpuHalt(void);

/**
Resets the CPU from the software level.
*/
void kmCpuSoftReset(void);

/**
Disables watchdog and interrupts. To be executed on startup in the *__init3* routine.
*/
void kmCpuDisableWatchdogAndInterruptsOnStartup(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* KMCPU_H_ */